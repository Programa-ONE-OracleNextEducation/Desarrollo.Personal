# **Productividad**

## **Enfocate en lo importante**

### **Preparando el ambiente**

Les dejamos disponible la presentación utilizada en el curso, para que puedan ir acompañando y haciendo sus apuntes a través de la misma.

Ver : Productividad-parte-1_Estrategias-para-actividades-diarias.zip

### **Para saber más**

Leí este artículo y me pareció una buena idea compartir contigo, pues creo que hay tips que puedes aplicar en tu dia a dia cuando enfrentes situaciones donde necesitas negociar.

[La solución de conflictos y el arte de negociar](https://www.harvard-deusto.com/la-solucion-de-conflictos-y-el-arte-de-negociar "Harvard deusto").

---

## **Haz lo correcto**

### **Preparando el ambiente**

Les dejamos disponible la presentación utilizada en clase, para que puedan ir acompañando y haciendo sus apuntes a través de la misma.

Ver : Conclusion.zip
