# **LinkedIn**

## **Tipos de perfiles**

### **Para saber más: Imágenes**

¿Tú foto de perfil transmite profesionalismo?

Toma un tiempo para revisar tu foto de perfil. Si no puedes tomar una nueva foto en este momento, organízate, pide ayuda a un amigo o amiga y toma una nueva foto que atienda las buenas prácticas que hemos visto en la clase.

¿Y para la foto de capa?

Si necesitas referencia de páginas de imágenes gratuitas de alta resolución, te dejo algunas opciones que puedes verificar:

- [Free Images](https://www.freeimages.com/es "Free Images").
- [Pixabay](https://pixabay.com/es/ "Pixabay").
- [Freepik](https://www.freepik.es/ "Freepik").
- [Morguefile](https://morguefile.com/ "Morguefile").

### **Para saber más: Toolkit**

Ver archivo : Toolkit+Linkedin.pdf

### **Para saber más: Portfolio**

Hemos visto que abajo del ítem “Acerca de” es posible agregar enlaces y archivos referentes a tu experiencia profesional, incluso también es posible insertar tu portfolio. Si te gusta la idea, pero aún no posees un portfolio, te voy a dejar un artículo de nuestro blog que puede ayudarte:

- [Cómo construir un portafolio de diseño](https://www.aluracursos.com/blog/como-construir-un-portafolio-de-design?utm_source=gnarus&utm_medium=timeline "Alura LATAM").

Si trabajas con otra área y sabes como crear un portfolio, comparta con nosotros en nuestro Foro.

### **Para saber más: Recomendaciones**

Las recomendaciones en LinkedIn son una estrategia muy buena para lograr tener una mayor visibilidad en la plataforma.

Para conseguir tener esas recomendaciones es muy importante aprender a solicitarlas de manera profesional.

- [Cómo hacer y pedir recomendaciones profesionales en LinkedIn](https://blog.hubspot.es/sales/escribir-recomendaciones-linkedin "Cómo hacer y pedir recomendaciones profesionales en LinkedIn").

---

## **Conexiones**

### **Haga lo que hicimos en aula**

Una vez que tu perfil ya esté más actualizado, llegó el momento de conectarse y empezar a crear tu red.

Si creaste tu cuenta recientemente, puedes darle click en “Mi red” para sincronizar tu correo con LinkedIn, y así, tus contactos recibirán invitaciones para conectarse contigo.

Entretanto, si ya tienes algunas conexiones, al darle click en “Mi red”, LinkedIn te sugerirá varios perfiles para que te conectes.

Puedes conectarte con compañeros y excompañeros de trabajo y de universidad, personas que trabajan en la misma empresa que tú, miembros de los grupos cuales participas.

Y recuérdate, cada vez que entres a un grupo nuevo observe si:

- ¿Las personas interactúan?
- ¿Las personas suben sus dudas?
- ¿Las publicaciones de este grupo son relevantes para tí?

---

## **Otras herramientas en Linkedin**

### **Para saber más: Planificación de tus artículos**

Hemos visto que es necesario ser constantes para subir artículos en el Pulse, pero sabemos que es difícil planificarse y organizarse para que podamos subir las publicaciones con cierta frecuencia.

Pensando en eso te dejo dos artículos disponibles aquí, los cuales te pueden ayudar con la organización de tus tareas.

- [5 formas de ayudarte a crear mejores hábitos](https://www.aluracursos.com/blog/5-formas-de-ayudarte-a-crear-mejores-habitos "5 formas de ayudarte a crear mejores hábitos").
- [Técnica Pomodoro: gestiona tu tiempo con sencillez](https://www.aluracursos.com/blog/tecnica-pomodoro-gestiona-tu-tiempo-con-sencillez "Técnica Pomodoro: gestiona tu tiempo con sencillez").

### **Haga lo que hicimos en aula**

**¿Qué tal compartir un post en tu Pulse?**

Realiza estos 4 pasos:

- Piensa en el mensaje general que quieres transmitir;
- Haz un esquema de tu artículo;
- Piensa en un título e imagen de capa;
- Formatea tu artículo para que sea más agradable de leer (párrafos, saltos de línea).

**¿Cuál es tu SSI?**

Ahora que ya conoces cómo mensurar tu LinkedIn puedes tomar una captura de pantalla y acompañar tu evolución al poner en práctica todo lo que aprendiste en este curso.

### **La importancia de LinkedIn para tu carrera profesional**

Espero que te haya gustado el contenido del curso. Te dejo aquí el link de un artículo en el blog de Alura Latam, que habla sobre todo lo que aprendimos en este curso y que te puede ser muy útil para recordar algunos tips muy importantes de LinkedIn.

- [La importancia de LinkedIn para tu carrera profesional](https://www.aluracursos.com/blog/la-importancia-de-linkedIn-para-tu-carrera-profesional "La importancia de LinkedIn para tu carrera profesional").

### **Haga lo que hicimos en aula**

¿Vamos a entrenar la creación de contenido?

Elige un tema pertinente a tu segmento y publica un pequeño artículo. Busca explorar los tips de:

- Haz preguntas interesantes;
- Utiliza llamadas de acción (Call to Action);
- Contenido relevante que se relacione con las tendencias;
- Reconoce / menciona tus compañeros de universidad / trabajo.
