# **Foco**

## **La ilusión de ser multitarea**

### **Principio de Pareto**

El llamado Principio de Pareto se aplica a la mayoría de las personas y empresas, nos ayuda a comprender que el tiempo es un recurso que no se usa adecuadamente de acuerdo con las prioridades correctas. Marcela necesita explicarle a su compañero de trabajo cuál es este principio. Según las alternativas a continuación, ¿cuál lo representa mejor?

Rta.

Cuando encuentras áreas que producen los mejores resultados y concentras la mayor parte de tu tiempo y energía en ellas. Si pasas la mayor parte de tu tiempo en reuniones improductivas, por ejemplo, estás perdiendo el tiempo que podrías usar para tareas realmente importantes que te traerían buenos resultados.

Aproximadamente el 80% de los efectos provienen del 20% de las causas. El enfoque en acciones minoritarias conduce a la mayoría de los resultados.

## **Fuerza de voluntad**

### **Acción**

Vimos que la fuerza de voluntad funciona como un músculo. Cuanto más practicamos un hábito, más fácil se vuelve. Pensando en eso, ¿qué puede hacer una persona para estimular su fuerza de voluntad?

Rta.

Buscar alternativas para encontrar lo que más le motiva. A veces queremos algo, pero sin pensar en qué hacer para lograrlo, terminamos desanimándonos. Entonces, lo ideal es pensar en alternativas para lograr estos objetivos.

Comenzar con desafíos menores. Los desafíos por sí solos ya son difíciles, por lo que para mantener la motivación, se recomienda comenzar con desafíos más pequeños y aumentarlos gradualmente.

## **La única cosa**

### **Pregunta de enfoque**

En esta clase vimos la importancia de hacer la pregunta de enfoque para ayudarnos a establecer prioridades y dedicarnos a lo más importante.

¿Cuál es la acción principal que puedo tomar para que, al hacerlo, el resto se vuelva más fácil o innecesario?

Para comenzar, concéntrate en las áreas principales de tu vida, a saber:

- Vida espiritual
- Salud física y mental
- Vida financiera
- La vida emocional

Si es posible, encuentra un lugar tranquilo donde puedas reflexionar sobre cada una de estas áreas y escribe las respuestas que se te ocurran. Luego evalúa si tienen sentido y cómo puedes ponerlas en práctica.

Rta.

Esta respuesta es muy personal, pero dentro del contexto en el que me encuentro hoy, sigue mis reflexiones:

- Vida espiritual: no me refiero a la religión, pero ¿qué puedo hacer para ayudar a los demás?
- Salud física: ¿cuál es la acción principal que puedo tomar para cumplir con mi rutina de ejercicios?
- Vida personal: ¿cuál es la acción principal que puedo tomar para mejorar mi capacidad de comunicarme con las personas?, o ¿qué es lo que puedo hacer para tener más tiempo para mí misma?
- Relaciones principales: ¿cuál es la acción principal que puedo tomar para mejorar mi relación con mi esposo(a), novio(a)? ¿Qué es lo principal que puedo hacer para demostrar que valoro a mis padres?
- Trabajo: ¿cuál es la acción principal que puedo hacer para ser reconocida?
- Negocios: ¿cuál es la acción principal que puedo tomar para mejorar mis cursos, mi proyecto de negocio, mi trabajo, mi emprendimiento?
- Vida financiera: ¿cuál es la acción principal que puedo tomar para aumentar mi patrimonio neto?
