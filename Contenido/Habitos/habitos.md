# **Hábitos**

## **Reconociendo tus habitos**

### **Tu meta SMART**

Ahora es tu turno de usar la Meta SMART. Después de establecer una meta, piensa en los 5 elementos que hacen una meta SMART y cómo puedes ajustarlos para que se cumpla.

Solo para recordar, una meta SMART debe ser: específica, medible, alcanzable, relevante y tener una fecha límite.

Ejemplo

Mi objetivo es aprender más sobre cómo hacer anuncios en Facebook.

- Especificidad: aprender a hacer anuncios en Facebook.
- Medible: hacer anuncios con la menor inversión y tener el mejor alcance.
- Alcanzable: hacer 10 anuncios por mes.
- Relevante: es relevante porque es un servicio que buscan mis clientes.
- Fecha límite: aprender esto en 2 meses.

---

## **Adios viejos habitos**

### **Elige el tuyo**

Cuando creamos un nuevo hábito, la recompensa puede motivarnos a mantenerlo. En esta clase vimos que Luisa, en lugar de pensar en alternativas exclusivas para cada tarea, cada vez que puede realizar una buena parte de sus tareas de la semana, va al cine.

¿Has pensado en las recompensas que quieres experimentar?

Si necesitas ayuda para pensar en diferentes recompensas, recuerda algunos puntos que cubrimos en este video:

- Juega tu juego favorito.
- Practica tu deporte favorito.
- Mira algo que te haga reír.
- Pasa tiempo con familiares / amigos.
- Relájate.
- Lee un libro diferente.

---

## **Autodisciplina**

### **Enfoque en ti**

Vimos que la disciplina es la capacidad de mantenerse enfocado en las tareas necesarias para lograr un objetivo sin desviarse ni perder la motivación.

### **Personas éxitosas**

Otra reflexión importante hecha en este video fue sobre lo que significa tener éxito. Esto puede variar de persona a persona. Tener una carrera estable, estar en constante aprendizaje, tener las finanzas actualizadas y esforzarse por superar las expectativas al entregar todo lo que se hace con calidad, puede ser uno de los criterios que indica cuándo una persona va a tener éxito.

Y en tu opinión:

- ¿Qué es ser una persona exitosa?
- ¿Qué piensas hacer para lograr o mantener este objetivo?

Recuerda que no hay correcto o incorrecto para este tipo de ejercicio, la idea es hacerte reflexionar sobre tu realidad en relación con las preguntas y, si lo prefieres, escríbelo en Evernote o en otro archivo para tener una referencia sobre tu respuesta.

Rta.

En mi opinión, ser una persona exitosa significa tener independencia y estabilidad financiera, ser libre de elegir con quién, dónde y cómo trabajar. Lo que pretendo hacer para mantener este objetivo es continuar estudiando, aprendiendo y buscando crear conexiones entre mis áreas de conocimiento con las demandas del mercado.
