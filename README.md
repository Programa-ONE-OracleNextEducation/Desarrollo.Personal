# **Desarrollo Personal**

## **¿Qué es lo que vas a aprender?**

En el mundo competitivo, conectado y dinámico de hoy las habilidades blandas o soft skills son competencias totalmente necesarias para tener un buen desempeño profesional. Es por ello que las empresas están preocupándose cada vez más por contratar personas con estas habilidades desarrolladas.

Es por ello que independientemente del área de actuación debemos preocuparnos por nuestro desarrollo personal y profesional en soft skills.

- **¿Cómo mejorar el desarrollo personal?**

Durante los cursos vas a conocer herramientas para promover tu foco, aprender conceptos como el loop del hábito, el método S.M.A.R.T, GTD (Getting Things Done) y aplicarlos para mejorar tu organización y desarrollar nuevos hábitos y mejorar tu desarrollo personal.

- **¿Cuál es la importancia del desarrollo personal?**

El desarrollo personal ayuda a establecer un mindset de alta performance, desarrollar la oratoria, mejorar las relaciones interpersonales y a entender como ofrecer y recibir feedbacks de la mejor forma.

Al concluir estar Formación habrás desarrollado habilidades que son claves para los nuevos desafíos del mercado de trabajo.

Esta formación forma parte del Programa ONE, una alianza entre Alura Latam y Oracle.

## **De quien vas a aprender**

- Priscila Stuani
- Gabriela Aguiar

## **Paso a paso**

### **1. Soft Skills**

Vas a desarrollar nuevos hábitos personales y profesionales y aprenderás métodos y estrategias que te ayudarán en tu productividad.

[La importancia de LinkedIn para tu carrera profesional](https://www.aluracursos.com/blog/la-importancia-de-linkedIn-para-tu-carrera-profesional "Alura LATAM").

[LinkedIn: Como hacer que tu perfil trabaje por ti](https://app.aluracursos.com/course/linkedin-hacer-perfil-trabaje-por-ti "LinkedIn"). Contenido [aqui](./Docs/linkedin.md).

[Planificando mis estudios](https://www.aluracursos.com/blog/planificando-mis-estudios "Alura LATAM").

[FOCO: Enfocarse trae mas resultados para el dia a dia](https://app.aluracursos.com/course/foco-habito-dia-a-dia "FOCO"). Contenido [aqui](./Docs/foco.md).

[5 formas de ayudarte a crear mejores hábitos](https://www.aluracursos.com/blog/5-formas-de-ayudarte-a-crear-mejores-habitos "Alura LATAM").

[Hábitos: Ser productivo para cumplir sus metas personales](https://app.aluracursos.com/course/habitos-productivo-metas-personales "Hábitos"). Contenido [aqui](./Docs/habitos.md).

[Técnica Pomodoro: gestiona tu tiempo con sencillez](https://www.aluracursos.com/blog/tecnica-pomodoro-gestiona-tu-tiempo-con-sencillez "Alura LATAM").

[Productividad parte 1: Estrategias para actividades diarias](https://app.aluracursos.com/course/productividad-parte-1-estrategias-actividades-diarias "Productividad parte 1"). Contenido [aqui](./Docs/productividad.md).

[5 pasos para una comunicación más asertiva](https://www.aluracursos.com/blog/5-pasos-para-una-comunicacion-mas-asertiva "Alura LATAM").

[¿Qué son las Soft Skills?](https://www.youtube.com/watch?v=vhwspfvI52k&ab_channel=AluraLatam "YouTube?").

### **2 Diagnóstico de lo aprendido**

Este paso es obligatorio Cuéntanos sobre tus conocimientos adquiridos hasta el momento. Es un diagnóstico personal y por ello es muy importante que seas sincero con tus respuestas.

[Ruta Des. Personal](https://grupocaelum.typeform.com/to/aaKkzvdi "Ruta Des. Personal").

## **Felicidades, completaste la formación Desarrollo Personal G3 - ONE**
