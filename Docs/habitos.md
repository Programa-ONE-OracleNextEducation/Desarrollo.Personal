## **Realice este curso para Productividad y Calidad de Vida y:**

- Sepa como crear metas que se adecuen a sus objetivos.
- Aprenda que es el Loop del Hábito y como usarlo para crear nuevos hábitos.
- Cree metas más efectivas con el método S.M.A.R.T.
- Descubra lo que son hábitos-clave y el motivo por el cual son tan importantes para su vida.
- Conozca la metodología GTD (Getting Things Done) y mejore su organización.

### **Aulas**

- **Reconociendo tus hábitos**

  - Presentación
  - Objetivos y metas
  - Ideas de objetivos
  - Resoluciones de año nuevo
  - Acción
  - El Loop del hábito
  - Componentes del hábito
  - Objetivos más específicos
  - Mejorando el hábito
  - Tu meta SMART
  - Lo que aprendimos

- **Hábitos Clave**

  - Cambio de habitos
  - Cambios
  - Habitos Clave
  - Nuevo hábito
  - Adquisición de nuevo hábito
  - Cambio de hábito
  - Observación
  - Obsérvate a ti mismo
  - Lo que aprendimos

- **Adios viejos hábitos**

  - Dilación
  - Recompensas
  - Conocimiento de sí mismo
  - Ahora es tu turno
  - Elige el tuyo
  - Lo que aprendimos

- **Autodisciplina**

  - Organización
  - Enfoque en ti
  - Estilo de vida
  - Personas éxitosas
  - Vida personal y profesional
  - Lo que aprendimos

- **Productividad**

  - Tecnica Pomodoro
  - Pomodoro
  - Gestionar interrupciones
  - Gestión
  - Indicación
  - Getting Things Done
  - Acción
  - Notas
  - Hábitos
  - Lo que aprendimos
  - Conclusión
