## **Realice este curso para Productividad y Calidad de Vida y:**

- Conozca herramientas para promover su foco
- Descubra cuáles son los principales villanos que perjudican su foco
- Sepa como evitar distracciones
- Ejercite su motivación
- Aprenda a usar el poder del hábito a favor de su foco

### **Aulas**

- **La ilusión de ser multitarea**

  - Presentación
  - El enemigo invisible
  - Principio de Pareto
  - Enfoque
  - Centrarse en lo que importa
  - Ahora es tu turno
  - Multitareas
  - Desafíos en la multitarea
  - Lo que aprendimos

- **Creencias**

  - Éxito y disciplina
  - Creencias
  - Cómo tener un mejor enfoque
  - Qué hay detrás del Eureka
  - Mapa mental
  - Reflexión y acción focalizada
  - Acción
  - Beneficios
  - Lo que aprendimos

- **Fuerza de voluntad**

  - El poder de la voluntad
  - Nuevos hábitos
  - Cómo mantener la fuerza de voluntad
  - Acción
  - Estrés
  - Lo que aprendimos

- **Equilibrio en la vida**

  - Preparando las cosas
  - Buenas prácticas
  - Bienestar
  - Centrarse en la vida personal
  - Estrategias
  - Lo que aprendimos

- **La única cosa**

  - El hábito del éxito
  - ¿Qué hay detrás de la pregunta focal?
  - Pregunta de enfoque
  - El poder del enfoque
  - Determinación
  - Lo que aprendimos
  - Conclusión
