## **Realice este curso para Marketing Personal y:**

- Descubre las mejores prácticas para destacar tu perfil
- Aprende a como dejar tu perfil más atractivo
- Conoce los principales recursos de LinkedIn y utilízalos a favor de tu carrera
- Descubre el poder de los contenidos a través de Pulse
- Aprende a como LinkedIn evalúa un perfil y mejora tus chances de aparecer en las búsquedas
- Crea una red de contactos de valor

### **Aulas**

- **Conociendo Linkedin**

  - Presentación
  - Conociendo el LinkedIn
  - Acciones
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Tipos de perfiles**

  - Perfil básico
  - Perfil básico informaciones adicionales
  - Perfil intermedio
  - Para saber más: Imágenes
  - Perfil avanzado
  - Para saber más: Toolkit
  - Haga lo que hicimos en aula
  - Para saber más: Portfolio
  - Para saber más: Recomendaciones
  - Lo que aprendimos

- **Conexiones**

  - Contactos
  - Grupos
  - Siguiendo personas y páginas empresariales
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Otras herramientas de Linkedin**

  - Pulse
  - Para saber más: Planificación de tus artículos
  - Qué es el SSI
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Contenido de calidad**

  - ¿Qué escribir?
  - La importancia de LinkedIn para tu carrera profesional
  - Haga lo que hicimos en aula
  - Lo que aprendimos
  - Conclusión
