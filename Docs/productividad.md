## **Realice este curso para Productividad y Calidad de Vida y:**

- Gestiona mejor tu vida personal y profesional
- Crea nuevos hábitos con enfoque en aumentar la productividad
- Administra mejor tu tiempo
- Descubre cómo priorizar mejor tus tareas
- Fomenta su capacidad de buscar nuevas oportunidades para ser más productivo

### **Aulas**

- **Enfócate en lo que importa**

  - Presentación
  - Preparando el ambiente
  - Piense en la solución
  - Para saber más
  - Solución
  - Problemas
  - Predecir el éxito
  - Establecer prioridades
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Define tu proposito**

  - Multiplica tu productividad
  - Metas
  - Acción
  - Empiece por lo más difícil
  - No te desvíes de la lista
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Productividad en la práctica**

  - Concéntrese en la actividad, no en el logro
  - Actividades Importantes
  - Reflexiona sobre las consecuencias
  - Eficiencia
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Procrastinación creativa**

  - 3 preguntas para maximizar la productividad
  - Alta productividad
  - Procrastinación creativa
  - Tareas
  - Haga lo que hicimos en aula
  - Lo que aprendimos

- **Haz lo correcto**

  - Preparando el ambiente
  - Conciencia
  - Clientes
  - Haga lo que hicimos en aula
  - Lo que aprendimos
  - Conclusión
